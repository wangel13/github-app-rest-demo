import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom'
import { Container } from '@mui/material'

import { Contributors } from './Routes/Contributors'
import { Profile } from './Routes/Profile'

function App() {
  return (
    <Router>
      <Container maxWidth="sm">
        <Switch>
          <Route path="/:id/:repo/contributors">
            <Contributors />
          </Route>
          <Route path="/:id">
            <Profile />
          </Route>
          <Route path="/">
            <Profile />
          </Route>
        </Switch>
      </Container>
    </Router>
  )
}

export default App
