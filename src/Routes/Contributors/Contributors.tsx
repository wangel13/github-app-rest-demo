import React from 'react'
import { Box, Link } from '@mui/material'
import { Link as RouterLink, useParams } from 'react-router-dom'
import { ContributorsList } from '../../Components/ContributorsList'
import { ArrowBack } from '@mui/icons-material'

function Contributors() {
  const { id, repo } = useParams<{ id: string; repo: string }>()

  return (
    <Box sx={{ my: 4 }}>
      <Box sx={{ mb: 4 }}>
        <Link component={RouterLink} to={`/${id}`}>
          <ArrowBack sx={{ verticalAlign: 'middle', mr: 1 }} fontSize="small" />
          back to {id}'s profile
        </Link>
      </Box>
      <ContributorsList login={id} repo={repo} />
    </Box>
  )
}

export default Contributors
