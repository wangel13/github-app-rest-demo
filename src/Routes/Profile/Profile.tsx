import React, { useEffect, useState } from 'react'
import { Box, TextField, Button, Grid } from '@mui/material'
import { useQuery } from 'react-query'
import { useHistory, useParams } from 'react-router-dom'
import getProfile from '../../models/profile/api/getProfile'
import { ProfileCard } from '../../Components/ProfileCard'
import { ReposList } from '../../Components/ReposList'

function Profile() {
  const { id = '' } = useParams<{ id: string }>()
  const history = useHistory()
  const [login, setLogin] = useState(id)

  const { data, refetch, isLoading } = useQuery(
    ['profile', login],
    () => getProfile(login),
    {
      refetchOnWindowFocus: false,
      enabled: false,
    }
  )

  useEffect(() => {
    if (login) {
      refetch()
    }
    // eslint-disable-next-line
  }, [id])

  const handleSearch = () => {
    history.push(`/${login}`)
  }

  const handleChangeLogin = (event: React.ChangeEvent<HTMLInputElement>) => {
    setLogin(event.target.value)
  }

  const handleLoginKeyUp = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter') {
      history.push(`/${login}`)
    }
  }

  return (
    <Box sx={{ my: 4 }}>
      <Grid container>
        <Grid item xs>
          <TextField
            fullWidth
            size="small"
            value={login}
            onKeyUp={handleLoginKeyUp}
            onChange={handleChangeLogin}
            label="Github login"
            variant="outlined"
          />
        </Grid>
        <Grid item>
          <Button
            onClick={handleSearch}
            disabled={isLoading}
            sx={{ ml: 1 }}
            variant="contained"
          >
            {isLoading ? 'Loading' : 'Search'}
          </Button>
        </Grid>
      </Grid>
      {data && !isLoading && (
        <Box sx={{ my: 4 }}>
          <ProfileCard profile={data} />
        </Box>
      )}

      {data && !isLoading && (
        <Box sx={{ my: 4 }}>
          <ReposList login={login} publicRepos={data?.public_repos} />
        </Box>
      )}
    </Box>
  )
}

export default Profile
