import axios from 'axios'
import { IContributor } from '../types'
import { getContributorsUrl } from './endpoints'

const getContributors = async (
  login: IContributor['login'],
  repo: string,
  query: any
): Promise<IContributor[]> => {
  const queryString = new URLSearchParams(query).toString()
  const response = await axios.get<IContributor[]>(
    `${getContributorsUrl(login, repo)}?${queryString}`
  )
  return response?.data
}

export default getContributors
