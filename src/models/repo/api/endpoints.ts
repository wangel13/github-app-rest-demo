import { getApiPrefix } from '../../getApiPrefix'

const Repos = '/repos'

export const getContributorsUrl = (login: string, repo: string) =>
  getApiPrefix(`${Repos}/${login}/${repo}/contributors`)
