export const getApiPrefix = (url: string) => {
  if (url.startsWith('http')) {
    return url
  }
  return `${process.env.REACT_APP_API_URL}${url}`
}
