import { getApiPrefix } from '../../getApiPrefix'

export const Profile = '/users'

export const getProfileUrl = (login: string) =>
  getApiPrefix(`${Profile}/${login}`)

export const getProfileReposUrl = (login: string) =>
  getApiPrefix(`${Profile}/${login}/repos`)
