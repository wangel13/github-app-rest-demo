import axios from 'axios'
import { IProfile } from '../types'
import { getProfileUrl } from './endpoints'

const getProfile = async (login: IProfile['login']): Promise<IProfile> => {
  const response = await axios.get<IProfile>(getProfileUrl(login))
  return response?.data
}

export default getProfile
