import axios from 'axios'
import { IProfile, IProfileRepo } from '../types'
import { getProfileReposUrl } from './endpoints'

const getProfileRepos = async (
  login: IProfile['login'],
  query: any
): Promise<IProfileRepo[]> => {
  const queryString = new URLSearchParams(query).toString()
  const response = await axios.get<IProfileRepo[]>(
    `${getProfileReposUrl(login)}?${queryString}`
  )
  return response?.data
}

export default getProfileRepos
