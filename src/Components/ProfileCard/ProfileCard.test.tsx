import React from 'react'
import { render, screen } from '@testing-library/react'
import ProfileCard from './ProfileCard'

const profile = {
  login: 'wangel13',
  id: 1074859,
  node_id: 'MDQ6VXNlcjEwNzQ4NTk=',
  avatar_url: 'https://avatars.githubusercontent.com/u/1074859?v=4',
  gravatar_id: '',
  url: 'https://api.github.com/users/wangel13',
  html_url: 'https://github.com/wangel13',
  followers_url: 'https://api.github.com/users/wangel13/followers',
  following_url: 'https://api.github.com/users/wangel13/following{/other_user}',
  gists_url: 'https://api.github.com/users/wangel13/gists{/gist_id}',
  starred_url: 'https://api.github.com/users/wangel13/starred{/owner}{/repo}',
  subscriptions_url: 'https://api.github.com/users/wangel13/subscriptions',
  organizations_url: 'https://api.github.com/users/wangel13/orgs',
  repos_url: 'https://api.github.com/users/wangel13/repos',
  events_url: 'https://api.github.com/users/wangel13/events{/privacy}',
  received_events_url: 'https://api.github.com/users/wangel13/received_events',
  type: 'User',
  site_admin: false,
  name: 'Taras Protchenko',
  company: null,
  blog: 'taras.one',
  location: null,
  email: null,
  hireable: true,
  bio: null,
  twitter_username: null,
  public_repos: 35,
  public_gists: 5,
  followers: 16,
  following: 29,
  created_at: '2011-09-23T19:05:29Z',
  updated_at: '2022-04-16T18:59:24Z',
}

test('displays profile card', () => {
  render(<ProfileCard profile={profile} />)
  const name = screen.getByText('Taras Protchenko')
  expect(name).toBeInTheDocument()
  expect(name).toHaveAttribute('href', 'https://github.com/wangel13')
  expect(screen.getByText('16 followers')).toBeInTheDocument()
  expect(screen.getByText('29 following')).toBeInTheDocument()
})
