import { LocationOn, People } from '@mui/icons-material'
import {
  Card,
  Box,
  Avatar,
  Typography,
  Divider,
  Grid,
  Link,
} from '@mui/material'
import { grey } from '@mui/material/colors'
import { FC } from 'react'
import { IProfile } from '../../models/profile/types'

type ProfileCardProps = {
  profile: IProfile
}

const ProfileCard: FC<ProfileCardProps> = ({ profile }) => {
  return (
    <Card>
      <Box sx={{ p: 2 }}>
        <Grid container wrap="wrap" spacing={1}>
          <Grid item>
            <Avatar src={profile?.avatar_url} sx={{ width: 56, height: 56 }} />
          </Grid>
          <Grid item xs>
            <Link
              href={`https://github.com/${profile?.login}`}
              target="_blank"
              variant="h5"
              fontWeight={700}
              underline="none"
              color="inherit"
              sx={{
                '&:hover': { opacity: 0.8 },
              }}
            >
              {profile?.name}
            </Link>
            <Typography variant="body2" color="text.secondary">
              <LocationOn
                sx={{ color: grey[400], verticalAlign: 'middle', mr: 0.5 }}
                fontSize="small"
              />
              {profile?.location ?? 'Location not set'}
            </Typography>
          </Grid>
        </Grid>
      </Box>
      <Divider />
      <Box sx={{ p: 2 }}>
        <People
          sx={{ color: grey[400], verticalAlign: 'middle', mr: 1 }}
          fontSize="small"
        />
        <Link
          href={`https://github.com/${profile?.login}?tab=followers`}
          target="_blank"
          variant="body2"
          underline="none"
          color="inherit"
          sx={{
            '&:hover': { opacity: 0.8 },
          }}
        >
          {profile?.followers} followers
        </Link>{' '}
        ·{' '}
        <Link
          href={`https://github.com/${profile?.login}?tab=following`}
          target="_blank"
          variant="body2"
          underline="none"
          color="inherit"
          sx={{
            '&:hover': { opacity: 0.8 },
          }}
        >
          {profile?.following} following
        </Link>
      </Box>
    </Card>
  )
}

export default ProfileCard
