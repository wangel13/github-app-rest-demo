import { Star, ForkRight, Visibility } from '@mui/icons-material'
import {
  Card,
  Box,
  Typography,
  Divider,
  Grid,
  Link,
  Chip,
  Button,
} from '@mui/material'
import { Link as RouterLink } from 'react-router-dom'
import { grey } from '@mui/material/colors'
import { FC } from 'react'
import { IProfileRepo } from '../../models/profile/types'

type RepoCardProps = {
  repo: IProfileRepo
}

const RepoCard: FC<RepoCardProps> = ({ repo }) => {
  return (
    <Card sx={{ mb: 2 }}>
      <Box sx={{ p: 2 }}>
        <Grid container wrap="wrap" spacing={1}>
          <Grid item xs>
            <Link
              href={repo?.html_url}
              target="_blank"
              variant="h5"
              fontWeight={700}
              underline="none"
              color="inherit"
              sx={{
                '&:hover': { opacity: 0.8 },
              }}
            >
              {repo?.name}
            </Link>
            <Typography variant="body2" color="text.secondary">
              {repo?.description ?? 'Description not set'}
            </Typography>
          </Grid>
          {repo?.language && (
            <Grid item>
              <Chip variant="outlined" color="info" label={repo?.language} />
            </Grid>
          )}
        </Grid>
        <Box sx={{ mt: 1 }}>
          <Button
            component={RouterLink}
            to={`/${repo?.full_name}/contributors`}
            size="small"
            variant="outlined"
          >
            Contributors
          </Button>
        </Box>
      </Box>
      <Divider />
      <Box sx={{ p: 2 }}>
        <Star
          sx={{ color: grey[400], verticalAlign: 'middle', mr: 1 }}
          fontSize="small"
        />
        <Link
          href={`${repo?.html_url}/stargazers`}
          target="_blank"
          variant="body2"
          underline="none"
          color="inherit"
          sx={{
            '&:hover': { opacity: 0.8 },
          }}
        >
          {repo?.stargazers_count} stargazers
        </Link>{' '}
        <Visibility
          sx={{ color: grey[400], verticalAlign: 'middle', mx: 1 }}
          fontSize="small"
        />
        <Link
          href={`${repo?.html_url}/watchers`}
          target="_blank"
          variant="body2"
          underline="none"
          color="inherit"
          sx={{
            '&:hover': { opacity: 0.8 },
          }}
        >
          {repo?.watchers_count} watchers
        </Link>{' '}
        <ForkRight
          sx={{ color: grey[400], verticalAlign: 'middle', mx: 1 }}
          fontSize="small"
        />
        <Link
          href={`${repo?.html_url}/network/members`}
          target="_blank"
          variant="body2"
          underline="none"
          color="inherit"
          sx={{
            '&:hover': { opacity: 0.8 },
          }}
        >
          {repo?.forks_count} forks
        </Link>
      </Box>
    </Card>
  )
}

export default RepoCard
