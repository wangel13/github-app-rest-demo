import React from 'react'
import { render, screen } from '@testing-library/react'
import ContributorCard from './ContributorCard'

const contributor = {
  login: 'wangel13',
  id: 1074859,
  node_id: 'MDQ6VXNlcjEwNzQ4NTk=',
  avatar_url: 'https://avatars.githubusercontent.com/u/1074859?v=4',
  gravatar_id: '',
  url: 'https://api.github.com/users/wangel13',
  html_url: 'https://github.com/wangel13',
  followers_url: 'https://api.github.com/users/wangel13/followers',
  following_url: 'https://api.github.com/users/wangel13/following{/other_user}',
  gists_url: 'https://api.github.com/users/wangel13/gists{/gist_id}',
  starred_url: 'https://api.github.com/users/wangel13/starred{/owner}{/repo}',
  subscriptions_url: 'https://api.github.com/users/wangel13/subscriptions',
  organizations_url: 'https://api.github.com/users/wangel13/orgs',
  repos_url: 'https://api.github.com/users/wangel13/repos',
  events_url: 'https://api.github.com/users/wangel13/events{/privacy}',
  received_events_url: 'https://api.github.com/users/wangel13/received_events',
  type: 'User',
  site_admin: false,
  contributions: 62,
}

test('displays contributor', () => {
  render(<ContributorCard contributor={contributor} />)
  expect(screen.getByText('wangel13')).toBeInTheDocument()
  expect(screen.getByTitle('contributions')).toHaveTextContent('62')
})
