import { Card, Box, Avatar, Typography, Grid, Link } from '@mui/material'
import { FC } from 'react'
import { IContributor } from '../../models/repo/types'

type ContributorCardProps = {
  contributor: IContributor
}

const ContributorCard: FC<ContributorCardProps> = ({ contributor }) => {
  return (
    <Card sx={{ mb: 2 }}>
      <Box sx={{ p: 2 }}>
        <Grid
          container
          wrap="wrap"
          spacing={1}
          justifyContent="center"
          alignItems="center"
        >
          <Grid item>
            <Avatar
              title={contributor?.login}
              src={contributor?.avatar_url}
              sx={{ width: 56, height: 56 }}
            />
          </Grid>
          <Grid item xs>
            <Link
              href={`https://github.com/${contributor?.login}`}
              target="_blank"
              variant="h5"
              fontWeight={700}
              underline="none"
              color="inherit"
              sx={{
                '&:hover': { opacity: 0.8 },
              }}
            >
              {contributor?.login}
            </Link>
          </Grid>
          <Grid item>
            <Typography
              title="contributions"
              variant="h4"
              color="text.secondary"
            >
              {contributor?.contributions}
            </Typography>
          </Grid>
        </Grid>
      </Box>
    </Card>
  )
}

export default ContributorCard
