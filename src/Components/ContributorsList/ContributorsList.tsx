import { Typography, TextField } from '@mui/material'
import React, { FC, useState } from 'react'
import { useQuery } from 'react-query'
import { IContributor } from '../../models/repo/types'
import getContributors from '../../models/repo/api/getContributors'
import { ContributorCard } from '../ContributorCard'

type ContributorsListProps = {
  login: IContributor['login']
  repo: string
}

const ContributorsList: FC<ContributorsListProps> = ({ login, repo }) => {
  const [searchLogin, setSearchLogin] = useState('')

  const { data = [], isLoading } = useQuery(['repos', login], () =>
    getContributors(login, repo, { sort: 'updated' })
  )

  const handleChangeLogin = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchLogin(event.target.value)
  }

  const filteredData: IContributor[] = data.filter((contributor) =>
    contributor?.login.startsWith(searchLogin)
  )

  if (isLoading) {
    return <>Contributors is loading...</>
  }

  return (
    <>
      <Typography variant="h6" sx={{ p: 2, pt: 0 }}>
        {repo} contributors:
      </Typography>
      <TextField
        sx={{ mb: 2 }}
        fullWidth
        size="small"
        value={searchLogin}
        onChange={handleChangeLogin}
        label="Contributor login"
        variant="outlined"
      />
      {filteredData
        ? filteredData.map((contributor: IContributor) => (
            <ContributorCard key={contributor?.id} contributor={contributor} />
          ))
        : 'No repos'}
    </>
  )
}

export default ContributorsList
