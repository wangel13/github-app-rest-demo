import React from 'react'
import axios from 'axios'
import { render, screen, waitFor, fireEvent } from '@testing-library/react'
import ContributorsList from './ContributorsList'
import { Wrapper } from '../../tests/Wrapper'

jest.mock('axios')
const axiosMock = axios as jest.Mocked<typeof axios>

afterEach(() => {
  axiosMock.get.mockReset()
})

const mockData = [
  {
    login: 'wangel13',
    id: 1074859,
    node_id: 'MDQ6VXNlcjEwNzQ4NTk=',
    avatar_url: 'https://avatars.githubusercontent.com/u/1074859?v=4',
    gravatar_id: '',
    url: 'https://api.github.com/users/wangel13',
    html_url: 'https://github.com/wangel13',
    followers_url: 'https://api.github.com/users/wangel13/followers',
    following_url:
      'https://api.github.com/users/wangel13/following{/other_user}',
    gists_url: 'https://api.github.com/users/wangel13/gists{/gist_id}',
    starred_url: 'https://api.github.com/users/wangel13/starred{/owner}{/repo}',
    subscriptions_url: 'https://api.github.com/users/wangel13/subscriptions',
    organizations_url: 'https://api.github.com/users/wangel13/orgs',
    repos_url: 'https://api.github.com/users/wangel13/repos',
    events_url: 'https://api.github.com/users/wangel13/events{/privacy}',
    received_events_url:
      'https://api.github.com/users/wangel13/received_events',
    type: 'User',
    site_admin: false,
    contributions: 62,
  },
  {
    login: 'renovate-bot',
    id: 25180681,
    node_id: 'MDQ6VXNlcjI1MTgwNjgx',
    avatar_url: 'https://avatars.githubusercontent.com/u/25180681?v=4',
    gravatar_id: '',
    url: 'https://api.github.com/users/renovate-bot',
    html_url: 'https://github.com/renovate-bot',
    followers_url: 'https://api.github.com/users/renovate-bot/followers',
    following_url:
      'https://api.github.com/users/renovate-bot/following{/other_user}',
    gists_url: 'https://api.github.com/users/renovate-bot/gists{/gist_id}',
    starred_url:
      'https://api.github.com/users/renovate-bot/starred{/owner}{/repo}',
    subscriptions_url:
      'https://api.github.com/users/renovate-bot/subscriptions',
    organizations_url: 'https://api.github.com/users/renovate-bot/orgs',
    repos_url: 'https://api.github.com/users/renovate-bot/repos',
    events_url: 'https://api.github.com/users/renovate-bot/events{/privacy}',
    received_events_url:
      'https://api.github.com/users/renovate-bot/received_events',
    type: 'User',
    site_admin: false,
    contributions: 50,
  },
]

test('displays contributors list', async () => {
  axiosMock.get.mockResolvedValue({
    status: 200,
    data: mockData,
  })
  render(
    <Wrapper>
      <ContributorsList
        login="wangel13"
        repo="prisma-next-auth-graphql-starter"
      />
    </Wrapper>
  )
  await waitFor(() => expect(screen.getByText('wangel13')).toBeInTheDocument())
  await waitFor(() =>
    expect(screen.getByText('renovate-bot')).toBeInTheDocument()
  )
})

test('displays filtered contributors list', async () => {
  axiosMock.get.mockResolvedValue({
    status: 200,
    data: mockData,
  })
  render(
    <Wrapper>
      <ContributorsList
        login="wangel13"
        repo="prisma-next-auth-graphql-starter"
      />
    </Wrapper>
  )
  await waitFor(() =>
    expect(screen.getByLabelText('Contributor login')).toBeInTheDocument()
  )
  const input = screen.getByLabelText('Contributor login')
  fireEvent.change(input, { target: { value: 'wangel13' } })
  await waitFor(() => expect(screen.getByText('wangel13')).toBeInTheDocument())
})

test('displays empty contributors list', async () => {
  axiosMock.get.mockResolvedValue({
    status: 200,
    data: mockData,
  })
  render(
    <Wrapper>
      <ContributorsList
        login="wangel13"
        repo="prisma-next-auth-graphql-starter"
      />
    </Wrapper>
  )
  await waitFor(() =>
    expect(screen.getByLabelText('Contributor login')).toBeInTheDocument()
  )
  const input = screen.getByLabelText('Contributor login')
  fireEvent.change(input, { target: { value: 'unknown' } })
  expect(screen.queryByText('unknown')).not.toBeInTheDocument()
})
