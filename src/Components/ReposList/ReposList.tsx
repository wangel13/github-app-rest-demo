import { Typography, Pagination } from '@mui/material'
import { ChangeEvent, FC, useState } from 'react'
import { IProfile, IProfileRepo } from '../../models/profile/types'
import { useQuery } from 'react-query'
import getProfileRepos from '../../models/profile/api/getProfileRepos'
import { RepoCard } from '../RepoCard'

type ReposListProps = {
  login: IProfile['login']
  publicRepos: number
}

const perPage = 5

const ReposList: FC<ReposListProps> = ({ login, publicRepos }) => {
  const [page, setPage] = useState(1)
  const pagesCount = (publicRepos / perPage) | 0

  const handleChangePage = (event: ChangeEvent<unknown>, value: number) => {
    setPage(value)
  }

  const { data, isLoading } = useQuery(['repos', login, page], () =>
    getProfileRepos(login, { page, per_page: perPage, sort: 'updated' })
  )

  if (isLoading) {
    return <div>repos is loading...</div>
  }

  return (
    <>
      <Typography variant="h6" sx={{ p: 2, pt: 0 }}>
        {login}'s repos:
      </Typography>
      {data
        ? data.map((repo: IProfileRepo) => (
            <RepoCard key={repo?.id} repo={repo} />
          ))
        : 'No repos'}
      <Pagination
        count={pagesCount}
        page={page}
        onChange={handleChangePage}
        variant="outlined"
      />
    </>
  )
}

export default ReposList
