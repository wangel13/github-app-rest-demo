import React from 'react'
import axios from 'axios'
import { render, screen, waitFor, fireEvent } from '@testing-library/react'
import ReposList from './ReposList'
import { Wrapper } from '../../tests/Wrapper'

jest.mock('axios')
const axiosMock = axios as jest.Mocked<typeof axios>

afterEach(() => {
  axiosMock.get.mockReset()
})

const mockData = [
  {
    id: 299659449,
    node_id: 'MDEwOlJlcG9zaXRvcnkyOTk2NTk0NDk=',
    name: 'prisma-next-auth-graphql-starter',
    full_name: 'wangel13/prisma-next-auth-graphql-starter',
    private: false,
    owner: {
      login: 'wangel13',
      id: 1074859,
      node_id: 'MDQ6VXNlcjEwNzQ4NTk=',
      avatar_url: 'https://avatars.githubusercontent.com/u/1074859?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/wangel13',
      html_url: 'https://github.com/wangel13',
      followers_url: 'https://api.github.com/users/wangel13/followers',
      following_url:
        'https://api.github.com/users/wangel13/following{/other_user}',
      gists_url: 'https://api.github.com/users/wangel13/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/wangel13/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/wangel13/subscriptions',
      organizations_url: 'https://api.github.com/users/wangel13/orgs',
      repos_url: 'https://api.github.com/users/wangel13/repos',
      events_url: 'https://api.github.com/users/wangel13/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/wangel13/received_events',
      type: 'User',
      site_admin: false,
    },
    html_url: 'https://github.com/wangel13/prisma-next-auth-graphql-starter',
    description:
      'Next.js boilerplate for passwordless authentication with Prisma and next-auth with apollo graphql api with style from tailwindcss',
    fork: false,
    url: 'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter',
    forks_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/forks',
    keys_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/collaborators{/collaborator}',
    teams_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/teams',
    hooks_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/hooks',
    issue_events_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/issues/events{/number}',
    events_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/events',
    assignees_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/branches{/branch}',
    tags_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/tags',
    blobs_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/languages',
    stargazers_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/stargazers',
    contributors_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/contributors',
    subscribers_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/subscribers',
    subscription_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/subscription',
    commits_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/compare/{base}...{head}',
    merges_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/merges',
    archive_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/downloads',
    issues_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/labels{/name}',
    releases_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/wangel13/prisma-next-auth-graphql-starter/deployments',
    created_at: '2020-09-29T15:30:04Z',
    updated_at: '2022-05-04T13:31:19Z',
    pushed_at: '2022-05-01T13:13:04Z',
    git_url: 'git://github.com/wangel13/prisma-next-auth-graphql-starter.git',
    ssh_url: 'git@github.com:wangel13/prisma-next-auth-graphql-starter.git',
    clone_url:
      'https://github.com/wangel13/prisma-next-auth-graphql-starter.git',
    svn_url: 'https://github.com/wangel13/prisma-next-auth-graphql-starter',
    homepage: '',
    size: 103,
    stargazers_count: 52,
    watchers_count: 52,
    language: 'TypeScript',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 7,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 1,
    license: {
      key: 'mit',
      name: 'MIT License',
      spdx_id: 'MIT',
      url: 'https://api.github.com/licenses/mit',
      node_id: 'MDc6TGljZW5zZTEz',
    },
    allow_forking: true,
    is_template: true,
    topics: [],
    visibility: 'public',
    forks: 7,
    open_issues: 1,
    watchers: 52,
    default_branch: 'main',
  },
  {
    id: 81250226,
    node_id: 'MDEwOlJlcG9zaXRvcnk4MTI1MDIyNg==',
    name: 'iTracker',
    full_name: 'wangel13/iTracker',
    private: false,
    owner: {
      login: 'wangel13',
      id: 1074859,
      node_id: 'MDQ6VXNlcjEwNzQ4NTk=',
      avatar_url: 'https://avatars.githubusercontent.com/u/1074859?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/wangel13',
      html_url: 'https://github.com/wangel13',
      followers_url: 'https://api.github.com/users/wangel13/followers',
      following_url:
        'https://api.github.com/users/wangel13/following{/other_user}',
      gists_url: 'https://api.github.com/users/wangel13/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/wangel13/starred{/owner}{/repo}',
      subscriptions_url: 'https://api.github.com/users/wangel13/subscriptions',
      organizations_url: 'https://api.github.com/users/wangel13/orgs',
      repos_url: 'https://api.github.com/users/wangel13/repos',
      events_url: 'https://api.github.com/users/wangel13/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/wangel13/received_events',
      type: 'User',
      site_admin: false,
    },
    html_url: 'https://github.com/wangel13/iTracker',
    description: null,
    fork: false,
    url: 'https://api.github.com/repos/wangel13/iTracker',
    forks_url: 'https://api.github.com/repos/wangel13/iTracker/forks',
    keys_url: 'https://api.github.com/repos/wangel13/iTracker/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/wangel13/iTracker/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/wangel13/iTracker/teams',
    hooks_url: 'https://api.github.com/repos/wangel13/iTracker/hooks',
    issue_events_url:
      'https://api.github.com/repos/wangel13/iTracker/issues/events{/number}',
    events_url: 'https://api.github.com/repos/wangel13/iTracker/events',
    assignees_url:
      'https://api.github.com/repos/wangel13/iTracker/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/wangel13/iTracker/branches{/branch}',
    tags_url: 'https://api.github.com/repos/wangel13/iTracker/tags',
    blobs_url: 'https://api.github.com/repos/wangel13/iTracker/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/wangel13/iTracker/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/wangel13/iTracker/git/refs{/sha}',
    trees_url: 'https://api.github.com/repos/wangel13/iTracker/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/wangel13/iTracker/statuses/{sha}',
    languages_url: 'https://api.github.com/repos/wangel13/iTracker/languages',
    stargazers_url: 'https://api.github.com/repos/wangel13/iTracker/stargazers',
    contributors_url:
      'https://api.github.com/repos/wangel13/iTracker/contributors',
    subscribers_url:
      'https://api.github.com/repos/wangel13/iTracker/subscribers',
    subscription_url:
      'https://api.github.com/repos/wangel13/iTracker/subscription',
    commits_url: 'https://api.github.com/repos/wangel13/iTracker/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/wangel13/iTracker/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/wangel13/iTracker/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/wangel13/iTracker/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/wangel13/iTracker/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/wangel13/iTracker/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/wangel13/iTracker/merges',
    archive_url:
      'https://api.github.com/repos/wangel13/iTracker/{archive_format}{/ref}',
    downloads_url: 'https://api.github.com/repos/wangel13/iTracker/downloads',
    issues_url:
      'https://api.github.com/repos/wangel13/iTracker/issues{/number}',
    pulls_url: 'https://api.github.com/repos/wangel13/iTracker/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/wangel13/iTracker/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/wangel13/iTracker/notifications{?since,all,participating}',
    labels_url: 'https://api.github.com/repos/wangel13/iTracker/labels{/name}',
    releases_url:
      'https://api.github.com/repos/wangel13/iTracker/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/wangel13/iTracker/deployments',
    created_at: '2017-02-07T20:21:18Z',
    updated_at: '2022-04-29T09:28:16Z',
    pushed_at: '2017-02-07T21:37:34Z',
    git_url: 'git://github.com/wangel13/iTracker.git',
    ssh_url: 'git@github.com:wangel13/iTracker.git',
    clone_url: 'https://github.com/wangel13/iTracker.git',
    svn_url: 'https://github.com/wangel13/iTracker',
    homepage: null,
    size: 49,
    stargazers_count: 1,
    watchers_count: 1,
    language: 'Ruby',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 0,
    mirror_url: null,
    archived: true,
    disabled: false,
    open_issues_count: 0,
    license: null,
    allow_forking: true,
    is_template: false,
    topics: [],
    visibility: 'public',
    forks: 0,
    open_issues: 0,
    watchers: 1,
    default_branch: 'master',
  },
]

test('displays user repos list', async () => {
  axiosMock.get.mockResolvedValue({
    status: 200,
    data: mockData,
  })
  render(
    <Wrapper>
      <ReposList login="wangel13" publicRepos={2} />
    </Wrapper>
  )
  await waitFor(() =>
    expect(
      screen.getByText('prisma-next-auth-graphql-starter')
    ).toBeInTheDocument()
  )
  expect(screen.getByText('iTracker')).toBeInTheDocument()
})
